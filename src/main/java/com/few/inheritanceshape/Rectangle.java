/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.inheritanceshape;

/**
 *
 * @author f
 */
public class Rectangle extends Shape{
    protected double w;
    protected double h;
    public Rectangle(double w, double h){
        this.w = w;
        this.h = h;
    }
    @Override
    public double calArea(){
        return w*h;
    }
}
