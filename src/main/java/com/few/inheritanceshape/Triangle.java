/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.inheritanceshape;

/**
 *
 * @author f
 */
public class Triangle extends Shape {
    private double b;
    private double h;
    private double m = 0.5;
    public Triangle (double b,double h){
        this.b = b;
        this.h = h;
    }
    @Override
    public double calArea(){
        return m*b*h;
    }
}
