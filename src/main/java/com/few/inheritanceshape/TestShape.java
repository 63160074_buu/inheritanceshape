/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.inheritanceshape;

/**
 *
 * @author f
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(2);
        System.out.println(circle.calArea());
        Triangle triangle = new Triangle(3,4);
        System.out.println(triangle.calArea());
        Rectangle rectangle = new Rectangle(3,4);
        System.out.println(rectangle.calArea());
        Square square = new Square(2);
        System.out.println(square.calArea());
    }
}
